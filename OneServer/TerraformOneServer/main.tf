terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  service_account_key_file = "/Users/ddronov/key.json"
  folder_id                = "b1gkveoveek2evque670"
  cloud_id                 = "b1glakdk9momi90hb5ob"
  zone                     = "ru-central1-b"
}

data "yandex_compute_image" "last_ubuntu" {
  family = "ubuntu-2004-lts" # ОС (Ubuntu, 20.04 LTS)
}

data "yandex_vpc_subnet" "default_b" {
  name = "default-ru-central1-b" # одна из дефолтных подсетей
}

resource "yandex_compute_instance" "my_webserver" {
  name = "terraform1"

  resources {
    cores         = 2
    memory        = 2
    core_fraction = 5
  }

  boot_disk {
    initialize_params {
      # ubuntu 20-04
      # image_id = "fd8k5kam36qhmnojj8je"
      image_id = data.yandex_compute_image.last_ubuntu.id
    }
  }

  network_interface {
    #subnet_id = "e2l80221lel3622urnc8"
    subnet_id = data.yandex_vpc_subnet.default_b.subnet_id
    nat       = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/yc_ed25519.pub")}"
  }
}

output "external_ip_address_vm_1" {
  value = yandex_compute_instance.my_webserver.network_interface.0.nat_ip_address
}
